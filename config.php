<?php

if(isset($_SERVER['IS_DEVEL']))
{
    $aConfig = [
        'PROTOCOL' => 'http',
        'ADMIN_PROTOCOL' => 'http',
        'CUSTOM_FOLDER' => 'NovumUwv',
        'ABSOLUTE_ROOT' => '/home/anton/Documents/sites/hurah',
        'DOMAIN' => 'uwv.demo.novum.nuidev.nl',
        /* Je zoekt waarschijnlijk Config::getDataDir() */
        'DATA_DIR' => '../'
    ];
}
else
{
    $aConfig = [
        'PROTOCOL' => 'https',
        'ADMIN_PROTOCOL' => 'https',
        'CUSTOM_FOLDER' => 'NovumUwv',
        'DOMAIN' => 'uwv.demo.novum.nu',
        'ABSOLUTE_ROOT' => '/home/nov_uwv/platform/system',
        'DATA_DIR' => '/home/nov_uwv/platform/data'
    ];
}

$aConfig['CUSTOM_NAMESPACE'] = 'NovumUwv';
return $aConfig;


