<?php
namespace AdminModules\Custom\NovumUwv\Werkloosheid\Iow\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\Custom\NovumUwv\Iow\CrudIowManager;
use Crud\FormManager;
use Model\Custom\NovumUwv\Iow;
use Model\Custom\NovumUwv\IowQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumUwv\Werkloosheid\Iow instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Wet inkomensvoorziening oudere werklozen";
	}


	public function getModule(): string
	{
		return "Iow";
	}


	public function getManager(): FormManager
	{
		return new CrudIowManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return IowQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof Iow){
		    LogActivity::register("Werkloosheid", "Wet inkomensvoorziening oudere werklozen verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Wet inkomensvoorziening oudere werklozen verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Wet inkomensvoorziening oudere werklozen niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Wet inkomensvoorziening oudere werklozen item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
