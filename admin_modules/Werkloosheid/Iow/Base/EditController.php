<?php
namespace AdminModules\Custom\NovumUwv\Werkloosheid\Iow\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumUwv\Iow\CrudIowManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumUwv\Werkloosheid\Iow instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudIowManager();
	}


	public function getPageTitle(): string
	{
		return "Wet inkomensvoorziening oudere werklozen";
	}
}
