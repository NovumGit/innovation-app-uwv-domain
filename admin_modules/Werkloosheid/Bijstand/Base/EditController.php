<?php
namespace AdminModules\Custom\NovumUwv\Werkloosheid\Bijstand\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumUwv\Bijstand\CrudBijstandManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumUwv\Werkloosheid\Bijstand instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudBijstandManager();
	}


	public function getPageTitle(): string
	{
		return "Bijstandswet";
	}
}
