<?php
namespace AdminModules\Custom\NovumUwv\Werkloosheid\Werkloosheidswet\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumUwv\Werkloosheidswet\CrudWerkloosheidswetManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumUwv\Werkloosheid\Werkloosheidswet instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudWerkloosheidswetManager();
	}


	public function getPageTitle(): string
	{
		return "Werkloosheidswet";
	}
}
