<?php
namespace AdminModules\Custom\NovumUwv\Arbeidsongeschiktheid\Wamil\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumUwv\wamil\CrudwamilManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumUwv\Arbeidsongeschiktheid\Wamil instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudwamilManager();
	}


	public function getPageTitle(): string
	{
		return "Wamil";
	}
}
