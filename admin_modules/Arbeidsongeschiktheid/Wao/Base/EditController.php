<?php
namespace AdminModules\Custom\NovumUwv\Arbeidsongeschiktheid\Wao\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumUwv\Wao\CrudWaoManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumUwv\Arbeidsongeschiktheid\Wao instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudWaoManager();
	}


	public function getPageTitle(): string
	{
		return "Wet arbeidsongeschiktheid";
	}
}
