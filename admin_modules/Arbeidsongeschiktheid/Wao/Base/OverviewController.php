<?php
namespace AdminModules\Custom\NovumUwv\Arbeidsongeschiktheid\Wao\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\Custom\NovumUwv\Wao\CrudWaoManager;
use Crud\FormManager;
use Model\Custom\NovumUwv\Wao;
use Model\Custom\NovumUwv\WaoQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumUwv\Arbeidsongeschiktheid\Wao instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Wet arbeidsongeschiktheid";
	}


	public function getModule(): string
	{
		return "Wao";
	}


	public function getManager(): FormManager
	{
		return new CrudWaoManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return WaoQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof Wao){
		    LogActivity::register("Arbeidsongeschiktheid", "Wet arbeidsongeschiktheid verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Wet arbeidsongeschiktheid verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Wet arbeidsongeschiktheid niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Wet arbeidsongeschiktheid item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
