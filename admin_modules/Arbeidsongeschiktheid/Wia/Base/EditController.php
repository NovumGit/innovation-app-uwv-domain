<?php
namespace AdminModules\Custom\NovumUwv\Arbeidsongeschiktheid\Wia\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumUwv\Wia\CrudWiaManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumUwv\Arbeidsongeschiktheid\Wia instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudWiaManager();
	}


	public function getPageTitle(): string
	{
		return "Wia";
	}
}
