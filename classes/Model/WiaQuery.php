<?php

namespace Model\Custom\NovumUwv;

use Model\Custom\NovumUwv\Base\WiaQuery as BaseWiaQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'wia' table.
 *
 * Bevat alle personen die gebruik maken van de WIA regeling
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class WiaQuery extends BaseWiaQuery
{

}
