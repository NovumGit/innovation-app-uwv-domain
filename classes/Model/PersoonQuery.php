<?php

namespace Model\Custom\NovumUwv;

use Model\Custom\NovumUwv\Base\PersoonQuery as BasePersoonQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'persoon' table.
 *
 * Alle bij het UWV bekendstaande personen
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PersoonQuery extends BasePersoonQuery
{

}
