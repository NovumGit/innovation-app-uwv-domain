<?php

namespace Model\Custom\NovumUwv;

use Model\Custom\NovumUwv\Base\Bijstand as BaseBijstand;

/**
 * Skeleton subclass for representing a row from the 'bijstand' table.
 *
 * Bevat alle personen die gebruik maken van de bijstandwet
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Bijstand extends BaseBijstand
{

}
