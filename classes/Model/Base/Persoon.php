<?php

namespace Model\Custom\NovumUwv\Base;

use \Exception;
use \PDO;
use Model\Custom\NovumUwv\Bijstand as ChildBijstand;
use Model\Custom\NovumUwv\BijstandQuery as ChildBijstandQuery;
use Model\Custom\NovumUwv\Iow as ChildIow;
use Model\Custom\NovumUwv\IowQuery as ChildIowQuery;
use Model\Custom\NovumUwv\Persoon as ChildPersoon;
use Model\Custom\NovumUwv\PersoonQuery as ChildPersoonQuery;
use Model\Custom\NovumUwv\Wao as ChildWao;
use Model\Custom\NovumUwv\WaoQuery as ChildWaoQuery;
use Model\Custom\NovumUwv\Werkloosheidswet as ChildWerkloosheidswet;
use Model\Custom\NovumUwv\WerkloosheidswetQuery as ChildWerkloosheidswetQuery;
use Model\Custom\NovumUwv\Wia as ChildWia;
use Model\Custom\NovumUwv\WiaQuery as ChildWiaQuery;
use Model\Custom\NovumUwv\wamil as Childwamil;
use Model\Custom\NovumUwv\wamilQuery as ChildwamilQuery;
use Model\Custom\NovumUwv\Map\BijstandTableMap;
use Model\Custom\NovumUwv\Map\IowTableMap;
use Model\Custom\NovumUwv\Map\PersoonTableMap;
use Model\Custom\NovumUwv\Map\WaoTableMap;
use Model\Custom\NovumUwv\Map\WerkloosheidswetTableMap;
use Model\Custom\NovumUwv\Map\WiaTableMap;
use Model\Custom\NovumUwv\Map\wamilTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'persoon' table.
 *
 * Alle bij het UWV bekendstaande personen
 *
 * @package    propel.generator.Model.Custom.NovumUwv.Base
 */
abstract class Persoon implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Custom\\NovumUwv\\Map\\PersoonTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the bsn field.
     *
     * @var        string
     */
    protected $bsn;

    /**
     * @var        ObjectCollection|ChildWerkloosheidswet[] Collection to store aggregation of ChildWerkloosheidswet objects.
     */
    protected $collWerkloosheidswets;
    protected $collWerkloosheidswetsPartial;

    /**
     * @var        ObjectCollection|ChildWao[] Collection to store aggregation of ChildWao objects.
     */
    protected $collWaos;
    protected $collWaosPartial;

    /**
     * @var        ObjectCollection|ChildWia[] Collection to store aggregation of ChildWia objects.
     */
    protected $collWias;
    protected $collWiasPartial;

    /**
     * @var        ObjectCollection|ChildIow[] Collection to store aggregation of ChildIow objects.
     */
    protected $collIows;
    protected $collIowsPartial;

    /**
     * @var        ObjectCollection|ChildBijstand[] Collection to store aggregation of ChildBijstand objects.
     */
    protected $collBijstands;
    protected $collBijstandsPartial;

    /**
     * @var        ObjectCollection|Childwamil[] Collection to store aggregation of Childwamil objects.
     */
    protected $collwamils;
    protected $collwamilsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildWerkloosheidswet[]
     */
    protected $werkloosheidswetsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildWao[]
     */
    protected $waosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildWia[]
     */
    protected $wiasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildIow[]
     */
    protected $iowsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBijstand[]
     */
    protected $bijstandsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Childwamil[]
     */
    protected $wamilsScheduledForDeletion = null;

    /**
     * Initializes internal state of Model\Custom\NovumUwv\Base\Persoon object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Persoon</code> instance.  If
     * <code>obj</code> is an instance of <code>Persoon</code>, delegates to
     * <code>equals(Persoon)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Persoon The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [bsn] column value.
     *
     * @return string
     */
    public function getBsn()
    {
        return $this->bsn;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumUwv\Persoon The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[PersoonTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [bsn] column.
     *
     * @param string $v new value
     * @return $this|\Model\Custom\NovumUwv\Persoon The current object (for fluent API support)
     */
    public function setBsn($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bsn !== $v) {
            $this->bsn = $v;
            $this->modifiedColumns[PersoonTableMap::COL_BSN] = true;
        }

        return $this;
    } // setBsn()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PersoonTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PersoonTableMap::translateFieldName('Bsn', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bsn = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 2; // 2 = PersoonTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Custom\\NovumUwv\\Persoon'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PersoonTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPersoonQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collWerkloosheidswets = null;

            $this->collWaos = null;

            $this->collWias = null;

            $this->collIows = null;

            $this->collBijstands = null;

            $this->collwamils = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Persoon::setDeleted()
     * @see Persoon::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PersoonTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPersoonQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PersoonTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PersoonTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->werkloosheidswetsScheduledForDeletion !== null) {
                if (!$this->werkloosheidswetsScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumUwv\WerkloosheidswetQuery::create()
                        ->filterByPrimaryKeys($this->werkloosheidswetsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->werkloosheidswetsScheduledForDeletion = null;
                }
            }

            if ($this->collWerkloosheidswets !== null) {
                foreach ($this->collWerkloosheidswets as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->waosScheduledForDeletion !== null) {
                if (!$this->waosScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumUwv\WaoQuery::create()
                        ->filterByPrimaryKeys($this->waosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->waosScheduledForDeletion = null;
                }
            }

            if ($this->collWaos !== null) {
                foreach ($this->collWaos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wiasScheduledForDeletion !== null) {
                if (!$this->wiasScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumUwv\WiaQuery::create()
                        ->filterByPrimaryKeys($this->wiasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wiasScheduledForDeletion = null;
                }
            }

            if ($this->collWias !== null) {
                foreach ($this->collWias as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->iowsScheduledForDeletion !== null) {
                if (!$this->iowsScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumUwv\IowQuery::create()
                        ->filterByPrimaryKeys($this->iowsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->iowsScheduledForDeletion = null;
                }
            }

            if ($this->collIows !== null) {
                foreach ($this->collIows as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bijstandsScheduledForDeletion !== null) {
                if (!$this->bijstandsScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumUwv\BijstandQuery::create()
                        ->filterByPrimaryKeys($this->bijstandsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->bijstandsScheduledForDeletion = null;
                }
            }

            if ($this->collBijstands !== null) {
                foreach ($this->collBijstands as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->wamilsScheduledForDeletion !== null) {
                if (!$this->wamilsScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumUwv\wamilQuery::create()
                        ->filterByPrimaryKeys($this->wamilsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->wamilsScheduledForDeletion = null;
                }
            }

            if ($this->collwamils !== null) {
                foreach ($this->collwamils as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PersoonTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PersoonTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PersoonTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(PersoonTableMap::COL_BSN)) {
            $modifiedColumns[':p' . $index++]  = 'bsn';
        }

        $sql = sprintf(
            'INSERT INTO persoon (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'bsn':
                        $stmt->bindValue($identifier, $this->bsn, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PersoonTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getBsn();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Persoon'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Persoon'][$this->hashCode()] = true;
        $keys = PersoonTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getBsn(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collWerkloosheidswets) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'werkloosheidswets';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'werkloosheidswets';
                        break;
                    default:
                        $key = 'Werkloosheidswets';
                }

                $result[$key] = $this->collWerkloosheidswets->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWaos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'waos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'waos';
                        break;
                    default:
                        $key = 'Waos';
                }

                $result[$key] = $this->collWaos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWias) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'wias';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'wias';
                        break;
                    default:
                        $key = 'Wias';
                }

                $result[$key] = $this->collWias->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collIows) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'iows';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'iows';
                        break;
                    default:
                        $key = 'Iows';
                }

                $result[$key] = $this->collIows->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBijstands) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bijstands';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'bijstands';
                        break;
                    default:
                        $key = 'Bijstands';
                }

                $result[$key] = $this->collBijstands->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collwamils) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'wamils';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'wamils';
                        break;
                    default:
                        $key = 'wamils';
                }

                $result[$key] = $this->collwamils->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Custom\NovumUwv\Persoon
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PersoonTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Custom\NovumUwv\Persoon
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setBsn($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PersoonTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setBsn($arr[$keys[1]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Custom\NovumUwv\Persoon The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PersoonTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PersoonTableMap::COL_ID)) {
            $criteria->add(PersoonTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(PersoonTableMap::COL_BSN)) {
            $criteria->add(PersoonTableMap::COL_BSN, $this->bsn);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPersoonQuery::create();
        $criteria->add(PersoonTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Custom\NovumUwv\Persoon (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBsn($this->getBsn());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getWerkloosheidswets() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWerkloosheidswet($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWaos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWao($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWias() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWia($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getIows() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addIow($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBijstands() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBijstand($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getwamils() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addwamil($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Custom\NovumUwv\Persoon Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Werkloosheidswet' == $relationName) {
            $this->initWerkloosheidswets();
            return;
        }
        if ('Wao' == $relationName) {
            $this->initWaos();
            return;
        }
        if ('Wia' == $relationName) {
            $this->initWias();
            return;
        }
        if ('Iow' == $relationName) {
            $this->initIows();
            return;
        }
        if ('Bijstand' == $relationName) {
            $this->initBijstands();
            return;
        }
        if ('wamil' == $relationName) {
            $this->initwamils();
            return;
        }
    }

    /**
     * Clears out the collWerkloosheidswets collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addWerkloosheidswets()
     */
    public function clearWerkloosheidswets()
    {
        $this->collWerkloosheidswets = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collWerkloosheidswets collection loaded partially.
     */
    public function resetPartialWerkloosheidswets($v = true)
    {
        $this->collWerkloosheidswetsPartial = $v;
    }

    /**
     * Initializes the collWerkloosheidswets collection.
     *
     * By default this just sets the collWerkloosheidswets collection to an empty array (like clearcollWerkloosheidswets());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWerkloosheidswets($overrideExisting = true)
    {
        if (null !== $this->collWerkloosheidswets && !$overrideExisting) {
            return;
        }

        $collectionClassName = WerkloosheidswetTableMap::getTableMap()->getCollectionClassName();

        $this->collWerkloosheidswets = new $collectionClassName;
        $this->collWerkloosheidswets->setModel('\Model\Custom\NovumUwv\Werkloosheidswet');
    }

    /**
     * Gets an array of ChildWerkloosheidswet objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersoon is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildWerkloosheidswet[] List of ChildWerkloosheidswet objects
     * @throws PropelException
     */
    public function getWerkloosheidswets(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collWerkloosheidswetsPartial && !$this->isNew();
        if (null === $this->collWerkloosheidswets || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWerkloosheidswets) {
                // return empty collection
                $this->initWerkloosheidswets();
            } else {
                $collWerkloosheidswets = ChildWerkloosheidswetQuery::create(null, $criteria)
                    ->filterByPersoon($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collWerkloosheidswetsPartial && count($collWerkloosheidswets)) {
                        $this->initWerkloosheidswets(false);

                        foreach ($collWerkloosheidswets as $obj) {
                            if (false == $this->collWerkloosheidswets->contains($obj)) {
                                $this->collWerkloosheidswets->append($obj);
                            }
                        }

                        $this->collWerkloosheidswetsPartial = true;
                    }

                    return $collWerkloosheidswets;
                }

                if ($partial && $this->collWerkloosheidswets) {
                    foreach ($this->collWerkloosheidswets as $obj) {
                        if ($obj->isNew()) {
                            $collWerkloosheidswets[] = $obj;
                        }
                    }
                }

                $this->collWerkloosheidswets = $collWerkloosheidswets;
                $this->collWerkloosheidswetsPartial = false;
            }
        }

        return $this->collWerkloosheidswets;
    }

    /**
     * Sets a collection of ChildWerkloosheidswet objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $werkloosheidswets A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function setWerkloosheidswets(Collection $werkloosheidswets, ConnectionInterface $con = null)
    {
        /** @var ChildWerkloosheidswet[] $werkloosheidswetsToDelete */
        $werkloosheidswetsToDelete = $this->getWerkloosheidswets(new Criteria(), $con)->diff($werkloosheidswets);


        $this->werkloosheidswetsScheduledForDeletion = $werkloosheidswetsToDelete;

        foreach ($werkloosheidswetsToDelete as $werkloosheidswetRemoved) {
            $werkloosheidswetRemoved->setPersoon(null);
        }

        $this->collWerkloosheidswets = null;
        foreach ($werkloosheidswets as $werkloosheidswet) {
            $this->addWerkloosheidswet($werkloosheidswet);
        }

        $this->collWerkloosheidswets = $werkloosheidswets;
        $this->collWerkloosheidswetsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Werkloosheidswet objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Werkloosheidswet objects.
     * @throws PropelException
     */
    public function countWerkloosheidswets(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collWerkloosheidswetsPartial && !$this->isNew();
        if (null === $this->collWerkloosheidswets || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWerkloosheidswets) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWerkloosheidswets());
            }

            $query = ChildWerkloosheidswetQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersoon($this)
                ->count($con);
        }

        return count($this->collWerkloosheidswets);
    }

    /**
     * Method called to associate a ChildWerkloosheidswet object to this object
     * through the ChildWerkloosheidswet foreign key attribute.
     *
     * @param  ChildWerkloosheidswet $l ChildWerkloosheidswet
     * @return $this|\Model\Custom\NovumUwv\Persoon The current object (for fluent API support)
     */
    public function addWerkloosheidswet(ChildWerkloosheidswet $l)
    {
        if ($this->collWerkloosheidswets === null) {
            $this->initWerkloosheidswets();
            $this->collWerkloosheidswetsPartial = true;
        }

        if (!$this->collWerkloosheidswets->contains($l)) {
            $this->doAddWerkloosheidswet($l);

            if ($this->werkloosheidswetsScheduledForDeletion and $this->werkloosheidswetsScheduledForDeletion->contains($l)) {
                $this->werkloosheidswetsScheduledForDeletion->remove($this->werkloosheidswetsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildWerkloosheidswet $werkloosheidswet The ChildWerkloosheidswet object to add.
     */
    protected function doAddWerkloosheidswet(ChildWerkloosheidswet $werkloosheidswet)
    {
        $this->collWerkloosheidswets[]= $werkloosheidswet;
        $werkloosheidswet->setPersoon($this);
    }

    /**
     * @param  ChildWerkloosheidswet $werkloosheidswet The ChildWerkloosheidswet object to remove.
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function removeWerkloosheidswet(ChildWerkloosheidswet $werkloosheidswet)
    {
        if ($this->getWerkloosheidswets()->contains($werkloosheidswet)) {
            $pos = $this->collWerkloosheidswets->search($werkloosheidswet);
            $this->collWerkloosheidswets->remove($pos);
            if (null === $this->werkloosheidswetsScheduledForDeletion) {
                $this->werkloosheidswetsScheduledForDeletion = clone $this->collWerkloosheidswets;
                $this->werkloosheidswetsScheduledForDeletion->clear();
            }
            $this->werkloosheidswetsScheduledForDeletion[]= clone $werkloosheidswet;
            $werkloosheidswet->setPersoon(null);
        }

        return $this;
    }

    /**
     * Clears out the collWaos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addWaos()
     */
    public function clearWaos()
    {
        $this->collWaos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collWaos collection loaded partially.
     */
    public function resetPartialWaos($v = true)
    {
        $this->collWaosPartial = $v;
    }

    /**
     * Initializes the collWaos collection.
     *
     * By default this just sets the collWaos collection to an empty array (like clearcollWaos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWaos($overrideExisting = true)
    {
        if (null !== $this->collWaos && !$overrideExisting) {
            return;
        }

        $collectionClassName = WaoTableMap::getTableMap()->getCollectionClassName();

        $this->collWaos = new $collectionClassName;
        $this->collWaos->setModel('\Model\Custom\NovumUwv\Wao');
    }

    /**
     * Gets an array of ChildWao objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersoon is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildWao[] List of ChildWao objects
     * @throws PropelException
     */
    public function getWaos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collWaosPartial && !$this->isNew();
        if (null === $this->collWaos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWaos) {
                // return empty collection
                $this->initWaos();
            } else {
                $collWaos = ChildWaoQuery::create(null, $criteria)
                    ->filterByPersoon($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collWaosPartial && count($collWaos)) {
                        $this->initWaos(false);

                        foreach ($collWaos as $obj) {
                            if (false == $this->collWaos->contains($obj)) {
                                $this->collWaos->append($obj);
                            }
                        }

                        $this->collWaosPartial = true;
                    }

                    return $collWaos;
                }

                if ($partial && $this->collWaos) {
                    foreach ($this->collWaos as $obj) {
                        if ($obj->isNew()) {
                            $collWaos[] = $obj;
                        }
                    }
                }

                $this->collWaos = $collWaos;
                $this->collWaosPartial = false;
            }
        }

        return $this->collWaos;
    }

    /**
     * Sets a collection of ChildWao objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $waos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function setWaos(Collection $waos, ConnectionInterface $con = null)
    {
        /** @var ChildWao[] $waosToDelete */
        $waosToDelete = $this->getWaos(new Criteria(), $con)->diff($waos);


        $this->waosScheduledForDeletion = $waosToDelete;

        foreach ($waosToDelete as $waoRemoved) {
            $waoRemoved->setPersoon(null);
        }

        $this->collWaos = null;
        foreach ($waos as $wao) {
            $this->addWao($wao);
        }

        $this->collWaos = $waos;
        $this->collWaosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Wao objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Wao objects.
     * @throws PropelException
     */
    public function countWaos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collWaosPartial && !$this->isNew();
        if (null === $this->collWaos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWaos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWaos());
            }

            $query = ChildWaoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersoon($this)
                ->count($con);
        }

        return count($this->collWaos);
    }

    /**
     * Method called to associate a ChildWao object to this object
     * through the ChildWao foreign key attribute.
     *
     * @param  ChildWao $l ChildWao
     * @return $this|\Model\Custom\NovumUwv\Persoon The current object (for fluent API support)
     */
    public function addWao(ChildWao $l)
    {
        if ($this->collWaos === null) {
            $this->initWaos();
            $this->collWaosPartial = true;
        }

        if (!$this->collWaos->contains($l)) {
            $this->doAddWao($l);

            if ($this->waosScheduledForDeletion and $this->waosScheduledForDeletion->contains($l)) {
                $this->waosScheduledForDeletion->remove($this->waosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildWao $wao The ChildWao object to add.
     */
    protected function doAddWao(ChildWao $wao)
    {
        $this->collWaos[]= $wao;
        $wao->setPersoon($this);
    }

    /**
     * @param  ChildWao $wao The ChildWao object to remove.
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function removeWao(ChildWao $wao)
    {
        if ($this->getWaos()->contains($wao)) {
            $pos = $this->collWaos->search($wao);
            $this->collWaos->remove($pos);
            if (null === $this->waosScheduledForDeletion) {
                $this->waosScheduledForDeletion = clone $this->collWaos;
                $this->waosScheduledForDeletion->clear();
            }
            $this->waosScheduledForDeletion[]= clone $wao;
            $wao->setPersoon(null);
        }

        return $this;
    }

    /**
     * Clears out the collWias collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addWias()
     */
    public function clearWias()
    {
        $this->collWias = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collWias collection loaded partially.
     */
    public function resetPartialWias($v = true)
    {
        $this->collWiasPartial = $v;
    }

    /**
     * Initializes the collWias collection.
     *
     * By default this just sets the collWias collection to an empty array (like clearcollWias());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWias($overrideExisting = true)
    {
        if (null !== $this->collWias && !$overrideExisting) {
            return;
        }

        $collectionClassName = WiaTableMap::getTableMap()->getCollectionClassName();

        $this->collWias = new $collectionClassName;
        $this->collWias->setModel('\Model\Custom\NovumUwv\Wia');
    }

    /**
     * Gets an array of ChildWia objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersoon is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildWia[] List of ChildWia objects
     * @throws PropelException
     */
    public function getWias(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collWiasPartial && !$this->isNew();
        if (null === $this->collWias || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWias) {
                // return empty collection
                $this->initWias();
            } else {
                $collWias = ChildWiaQuery::create(null, $criteria)
                    ->filterByPersoon($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collWiasPartial && count($collWias)) {
                        $this->initWias(false);

                        foreach ($collWias as $obj) {
                            if (false == $this->collWias->contains($obj)) {
                                $this->collWias->append($obj);
                            }
                        }

                        $this->collWiasPartial = true;
                    }

                    return $collWias;
                }

                if ($partial && $this->collWias) {
                    foreach ($this->collWias as $obj) {
                        if ($obj->isNew()) {
                            $collWias[] = $obj;
                        }
                    }
                }

                $this->collWias = $collWias;
                $this->collWiasPartial = false;
            }
        }

        return $this->collWias;
    }

    /**
     * Sets a collection of ChildWia objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $wias A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function setWias(Collection $wias, ConnectionInterface $con = null)
    {
        /** @var ChildWia[] $wiasToDelete */
        $wiasToDelete = $this->getWias(new Criteria(), $con)->diff($wias);


        $this->wiasScheduledForDeletion = $wiasToDelete;

        foreach ($wiasToDelete as $wiaRemoved) {
            $wiaRemoved->setPersoon(null);
        }

        $this->collWias = null;
        foreach ($wias as $wia) {
            $this->addWia($wia);
        }

        $this->collWias = $wias;
        $this->collWiasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Wia objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Wia objects.
     * @throws PropelException
     */
    public function countWias(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collWiasPartial && !$this->isNew();
        if (null === $this->collWias || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWias) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWias());
            }

            $query = ChildWiaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersoon($this)
                ->count($con);
        }

        return count($this->collWias);
    }

    /**
     * Method called to associate a ChildWia object to this object
     * through the ChildWia foreign key attribute.
     *
     * @param  ChildWia $l ChildWia
     * @return $this|\Model\Custom\NovumUwv\Persoon The current object (for fluent API support)
     */
    public function addWia(ChildWia $l)
    {
        if ($this->collWias === null) {
            $this->initWias();
            $this->collWiasPartial = true;
        }

        if (!$this->collWias->contains($l)) {
            $this->doAddWia($l);

            if ($this->wiasScheduledForDeletion and $this->wiasScheduledForDeletion->contains($l)) {
                $this->wiasScheduledForDeletion->remove($this->wiasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildWia $wia The ChildWia object to add.
     */
    protected function doAddWia(ChildWia $wia)
    {
        $this->collWias[]= $wia;
        $wia->setPersoon($this);
    }

    /**
     * @param  ChildWia $wia The ChildWia object to remove.
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function removeWia(ChildWia $wia)
    {
        if ($this->getWias()->contains($wia)) {
            $pos = $this->collWias->search($wia);
            $this->collWias->remove($pos);
            if (null === $this->wiasScheduledForDeletion) {
                $this->wiasScheduledForDeletion = clone $this->collWias;
                $this->wiasScheduledForDeletion->clear();
            }
            $this->wiasScheduledForDeletion[]= clone $wia;
            $wia->setPersoon(null);
        }

        return $this;
    }

    /**
     * Clears out the collIows collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addIows()
     */
    public function clearIows()
    {
        $this->collIows = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collIows collection loaded partially.
     */
    public function resetPartialIows($v = true)
    {
        $this->collIowsPartial = $v;
    }

    /**
     * Initializes the collIows collection.
     *
     * By default this just sets the collIows collection to an empty array (like clearcollIows());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initIows($overrideExisting = true)
    {
        if (null !== $this->collIows && !$overrideExisting) {
            return;
        }

        $collectionClassName = IowTableMap::getTableMap()->getCollectionClassName();

        $this->collIows = new $collectionClassName;
        $this->collIows->setModel('\Model\Custom\NovumUwv\Iow');
    }

    /**
     * Gets an array of ChildIow objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersoon is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildIow[] List of ChildIow objects
     * @throws PropelException
     */
    public function getIows(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collIowsPartial && !$this->isNew();
        if (null === $this->collIows || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collIows) {
                // return empty collection
                $this->initIows();
            } else {
                $collIows = ChildIowQuery::create(null, $criteria)
                    ->filterByPersoon($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collIowsPartial && count($collIows)) {
                        $this->initIows(false);

                        foreach ($collIows as $obj) {
                            if (false == $this->collIows->contains($obj)) {
                                $this->collIows->append($obj);
                            }
                        }

                        $this->collIowsPartial = true;
                    }

                    return $collIows;
                }

                if ($partial && $this->collIows) {
                    foreach ($this->collIows as $obj) {
                        if ($obj->isNew()) {
                            $collIows[] = $obj;
                        }
                    }
                }

                $this->collIows = $collIows;
                $this->collIowsPartial = false;
            }
        }

        return $this->collIows;
    }

    /**
     * Sets a collection of ChildIow objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $iows A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function setIows(Collection $iows, ConnectionInterface $con = null)
    {
        /** @var ChildIow[] $iowsToDelete */
        $iowsToDelete = $this->getIows(new Criteria(), $con)->diff($iows);


        $this->iowsScheduledForDeletion = $iowsToDelete;

        foreach ($iowsToDelete as $iowRemoved) {
            $iowRemoved->setPersoon(null);
        }

        $this->collIows = null;
        foreach ($iows as $iow) {
            $this->addIow($iow);
        }

        $this->collIows = $iows;
        $this->collIowsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Iow objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Iow objects.
     * @throws PropelException
     */
    public function countIows(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collIowsPartial && !$this->isNew();
        if (null === $this->collIows || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collIows) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getIows());
            }

            $query = ChildIowQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersoon($this)
                ->count($con);
        }

        return count($this->collIows);
    }

    /**
     * Method called to associate a ChildIow object to this object
     * through the ChildIow foreign key attribute.
     *
     * @param  ChildIow $l ChildIow
     * @return $this|\Model\Custom\NovumUwv\Persoon The current object (for fluent API support)
     */
    public function addIow(ChildIow $l)
    {
        if ($this->collIows === null) {
            $this->initIows();
            $this->collIowsPartial = true;
        }

        if (!$this->collIows->contains($l)) {
            $this->doAddIow($l);

            if ($this->iowsScheduledForDeletion and $this->iowsScheduledForDeletion->contains($l)) {
                $this->iowsScheduledForDeletion->remove($this->iowsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildIow $iow The ChildIow object to add.
     */
    protected function doAddIow(ChildIow $iow)
    {
        $this->collIows[]= $iow;
        $iow->setPersoon($this);
    }

    /**
     * @param  ChildIow $iow The ChildIow object to remove.
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function removeIow(ChildIow $iow)
    {
        if ($this->getIows()->contains($iow)) {
            $pos = $this->collIows->search($iow);
            $this->collIows->remove($pos);
            if (null === $this->iowsScheduledForDeletion) {
                $this->iowsScheduledForDeletion = clone $this->collIows;
                $this->iowsScheduledForDeletion->clear();
            }
            $this->iowsScheduledForDeletion[]= clone $iow;
            $iow->setPersoon(null);
        }

        return $this;
    }

    /**
     * Clears out the collBijstands collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBijstands()
     */
    public function clearBijstands()
    {
        $this->collBijstands = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBijstands collection loaded partially.
     */
    public function resetPartialBijstands($v = true)
    {
        $this->collBijstandsPartial = $v;
    }

    /**
     * Initializes the collBijstands collection.
     *
     * By default this just sets the collBijstands collection to an empty array (like clearcollBijstands());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBijstands($overrideExisting = true)
    {
        if (null !== $this->collBijstands && !$overrideExisting) {
            return;
        }

        $collectionClassName = BijstandTableMap::getTableMap()->getCollectionClassName();

        $this->collBijstands = new $collectionClassName;
        $this->collBijstands->setModel('\Model\Custom\NovumUwv\Bijstand');
    }

    /**
     * Gets an array of ChildBijstand objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersoon is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBijstand[] List of ChildBijstand objects
     * @throws PropelException
     */
    public function getBijstands(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBijstandsPartial && !$this->isNew();
        if (null === $this->collBijstands || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBijstands) {
                // return empty collection
                $this->initBijstands();
            } else {
                $collBijstands = ChildBijstandQuery::create(null, $criteria)
                    ->filterByPersoon($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBijstandsPartial && count($collBijstands)) {
                        $this->initBijstands(false);

                        foreach ($collBijstands as $obj) {
                            if (false == $this->collBijstands->contains($obj)) {
                                $this->collBijstands->append($obj);
                            }
                        }

                        $this->collBijstandsPartial = true;
                    }

                    return $collBijstands;
                }

                if ($partial && $this->collBijstands) {
                    foreach ($this->collBijstands as $obj) {
                        if ($obj->isNew()) {
                            $collBijstands[] = $obj;
                        }
                    }
                }

                $this->collBijstands = $collBijstands;
                $this->collBijstandsPartial = false;
            }
        }

        return $this->collBijstands;
    }

    /**
     * Sets a collection of ChildBijstand objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $bijstands A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function setBijstands(Collection $bijstands, ConnectionInterface $con = null)
    {
        /** @var ChildBijstand[] $bijstandsToDelete */
        $bijstandsToDelete = $this->getBijstands(new Criteria(), $con)->diff($bijstands);


        $this->bijstandsScheduledForDeletion = $bijstandsToDelete;

        foreach ($bijstandsToDelete as $bijstandRemoved) {
            $bijstandRemoved->setPersoon(null);
        }

        $this->collBijstands = null;
        foreach ($bijstands as $bijstand) {
            $this->addBijstand($bijstand);
        }

        $this->collBijstands = $bijstands;
        $this->collBijstandsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Bijstand objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Bijstand objects.
     * @throws PropelException
     */
    public function countBijstands(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBijstandsPartial && !$this->isNew();
        if (null === $this->collBijstands || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBijstands) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBijstands());
            }

            $query = ChildBijstandQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersoon($this)
                ->count($con);
        }

        return count($this->collBijstands);
    }

    /**
     * Method called to associate a ChildBijstand object to this object
     * through the ChildBijstand foreign key attribute.
     *
     * @param  ChildBijstand $l ChildBijstand
     * @return $this|\Model\Custom\NovumUwv\Persoon The current object (for fluent API support)
     */
    public function addBijstand(ChildBijstand $l)
    {
        if ($this->collBijstands === null) {
            $this->initBijstands();
            $this->collBijstandsPartial = true;
        }

        if (!$this->collBijstands->contains($l)) {
            $this->doAddBijstand($l);

            if ($this->bijstandsScheduledForDeletion and $this->bijstandsScheduledForDeletion->contains($l)) {
                $this->bijstandsScheduledForDeletion->remove($this->bijstandsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBijstand $bijstand The ChildBijstand object to add.
     */
    protected function doAddBijstand(ChildBijstand $bijstand)
    {
        $this->collBijstands[]= $bijstand;
        $bijstand->setPersoon($this);
    }

    /**
     * @param  ChildBijstand $bijstand The ChildBijstand object to remove.
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function removeBijstand(ChildBijstand $bijstand)
    {
        if ($this->getBijstands()->contains($bijstand)) {
            $pos = $this->collBijstands->search($bijstand);
            $this->collBijstands->remove($pos);
            if (null === $this->bijstandsScheduledForDeletion) {
                $this->bijstandsScheduledForDeletion = clone $this->collBijstands;
                $this->bijstandsScheduledForDeletion->clear();
            }
            $this->bijstandsScheduledForDeletion[]= clone $bijstand;
            $bijstand->setPersoon(null);
        }

        return $this;
    }

    /**
     * Clears out the collwamils collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addwamils()
     */
    public function clearwamils()
    {
        $this->collwamils = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collwamils collection loaded partially.
     */
    public function resetPartialwamils($v = true)
    {
        $this->collwamilsPartial = $v;
    }

    /**
     * Initializes the collwamils collection.
     *
     * By default this just sets the collwamils collection to an empty array (like clearcollwamils());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initwamils($overrideExisting = true)
    {
        if (null !== $this->collwamils && !$overrideExisting) {
            return;
        }

        $collectionClassName = wamilTableMap::getTableMap()->getCollectionClassName();

        $this->collwamils = new $collectionClassName;
        $this->collwamils->setModel('\Model\Custom\NovumUwv\wamil');
    }

    /**
     * Gets an array of Childwamil objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersoon is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Childwamil[] List of Childwamil objects
     * @throws PropelException
     */
    public function getwamils(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collwamilsPartial && !$this->isNew();
        if (null === $this->collwamils || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collwamils) {
                // return empty collection
                $this->initwamils();
            } else {
                $collwamils = ChildwamilQuery::create(null, $criteria)
                    ->filterByPersoon($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collwamilsPartial && count($collwamils)) {
                        $this->initwamils(false);

                        foreach ($collwamils as $obj) {
                            if (false == $this->collwamils->contains($obj)) {
                                $this->collwamils->append($obj);
                            }
                        }

                        $this->collwamilsPartial = true;
                    }

                    return $collwamils;
                }

                if ($partial && $this->collwamils) {
                    foreach ($this->collwamils as $obj) {
                        if ($obj->isNew()) {
                            $collwamils[] = $obj;
                        }
                    }
                }

                $this->collwamils = $collwamils;
                $this->collwamilsPartial = false;
            }
        }

        return $this->collwamils;
    }

    /**
     * Sets a collection of Childwamil objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $wamils A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function setwamils(Collection $wamils, ConnectionInterface $con = null)
    {
        /** @var Childwamil[] $wamilsToDelete */
        $wamilsToDelete = $this->getwamils(new Criteria(), $con)->diff($wamils);


        $this->wamilsScheduledForDeletion = $wamilsToDelete;

        foreach ($wamilsToDelete as $wamilRemoved) {
            $wamilRemoved->setPersoon(null);
        }

        $this->collwamils = null;
        foreach ($wamils as $wamil) {
            $this->addwamil($wamil);
        }

        $this->collwamils = $wamils;
        $this->collwamilsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related wamil objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related wamil objects.
     * @throws PropelException
     */
    public function countwamils(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collwamilsPartial && !$this->isNew();
        if (null === $this->collwamils || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collwamils) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getwamils());
            }

            $query = ChildwamilQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersoon($this)
                ->count($con);
        }

        return count($this->collwamils);
    }

    /**
     * Method called to associate a Childwamil object to this object
     * through the Childwamil foreign key attribute.
     *
     * @param  Childwamil $l Childwamil
     * @return $this|\Model\Custom\NovumUwv\Persoon The current object (for fluent API support)
     */
    public function addwamil(Childwamil $l)
    {
        if ($this->collwamils === null) {
            $this->initwamils();
            $this->collwamilsPartial = true;
        }

        if (!$this->collwamils->contains($l)) {
            $this->doAddwamil($l);

            if ($this->wamilsScheduledForDeletion and $this->wamilsScheduledForDeletion->contains($l)) {
                $this->wamilsScheduledForDeletion->remove($this->wamilsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Childwamil $wamil The Childwamil object to add.
     */
    protected function doAddwamil(Childwamil $wamil)
    {
        $this->collwamils[]= $wamil;
        $wamil->setPersoon($this);
    }

    /**
     * @param  Childwamil $wamil The Childwamil object to remove.
     * @return $this|ChildPersoon The current object (for fluent API support)
     */
    public function removewamil(Childwamil $wamil)
    {
        if ($this->getwamils()->contains($wamil)) {
            $pos = $this->collwamils->search($wamil);
            $this->collwamils->remove($pos);
            if (null === $this->wamilsScheduledForDeletion) {
                $this->wamilsScheduledForDeletion = clone $this->collwamils;
                $this->wamilsScheduledForDeletion->clear();
            }
            $this->wamilsScheduledForDeletion[]= clone $wamil;
            $wamil->setPersoon(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->bsn = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collWerkloosheidswets) {
                foreach ($this->collWerkloosheidswets as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWaos) {
                foreach ($this->collWaos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWias) {
                foreach ($this->collWias as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collIows) {
                foreach ($this->collIows as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBijstands) {
                foreach ($this->collBijstands as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collwamils) {
                foreach ($this->collwamils as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collWerkloosheidswets = null;
        $this->collWaos = null;
        $this->collWias = null;
        $this->collIows = null;
        $this->collBijstands = null;
        $this->collwamils = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PersoonTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
