<?php

namespace Model\Custom\NovumUwv\Base;

use \Exception;
use \PDO;
use Model\Custom\NovumUwv\Werkloosheidswet as ChildWerkloosheidswet;
use Model\Custom\NovumUwv\WerkloosheidswetQuery as ChildWerkloosheidswetQuery;
use Model\Custom\NovumUwv\Map\WerkloosheidswetTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'werkloosheidswet' table.
 *
 * Bevat alle personen die gebruik maken van de werkloosheidswet
 *
 * @method     ChildWerkloosheidswetQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildWerkloosheidswetQuery orderByPersoonId($order = Criteria::ASC) Order by the persoon_id column
 * @method     ChildWerkloosheidswetQuery orderByBedrag($order = Criteria::ASC) Order by the bedrag column
 * @method     ChildWerkloosheidswetQuery orderByStartDatum($order = Criteria::ASC) Order by the start_datum column
 *
 * @method     ChildWerkloosheidswetQuery groupById() Group by the id column
 * @method     ChildWerkloosheidswetQuery groupByPersoonId() Group by the persoon_id column
 * @method     ChildWerkloosheidswetQuery groupByBedrag() Group by the bedrag column
 * @method     ChildWerkloosheidswetQuery groupByStartDatum() Group by the start_datum column
 *
 * @method     ChildWerkloosheidswetQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildWerkloosheidswetQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildWerkloosheidswetQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildWerkloosheidswetQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildWerkloosheidswetQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildWerkloosheidswetQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildWerkloosheidswetQuery leftJoinPersoon($relationAlias = null) Adds a LEFT JOIN clause to the query using the Persoon relation
 * @method     ChildWerkloosheidswetQuery rightJoinPersoon($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Persoon relation
 * @method     ChildWerkloosheidswetQuery innerJoinPersoon($relationAlias = null) Adds a INNER JOIN clause to the query using the Persoon relation
 *
 * @method     ChildWerkloosheidswetQuery joinWithPersoon($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Persoon relation
 *
 * @method     ChildWerkloosheidswetQuery leftJoinWithPersoon() Adds a LEFT JOIN clause and with to the query using the Persoon relation
 * @method     ChildWerkloosheidswetQuery rightJoinWithPersoon() Adds a RIGHT JOIN clause and with to the query using the Persoon relation
 * @method     ChildWerkloosheidswetQuery innerJoinWithPersoon() Adds a INNER JOIN clause and with to the query using the Persoon relation
 *
 * @method     \Model\Custom\NovumUwv\PersoonQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildWerkloosheidswet findOne(ConnectionInterface $con = null) Return the first ChildWerkloosheidswet matching the query
 * @method     ChildWerkloosheidswet findOneOrCreate(ConnectionInterface $con = null) Return the first ChildWerkloosheidswet matching the query, or a new ChildWerkloosheidswet object populated from the query conditions when no match is found
 *
 * @method     ChildWerkloosheidswet findOneById(int $id) Return the first ChildWerkloosheidswet filtered by the id column
 * @method     ChildWerkloosheidswet findOneByPersoonId(int $persoon_id) Return the first ChildWerkloosheidswet filtered by the persoon_id column
 * @method     ChildWerkloosheidswet findOneByBedrag(string $bedrag) Return the first ChildWerkloosheidswet filtered by the bedrag column
 * @method     ChildWerkloosheidswet findOneByStartDatum(string $start_datum) Return the first ChildWerkloosheidswet filtered by the start_datum column *

 * @method     ChildWerkloosheidswet requirePk($key, ConnectionInterface $con = null) Return the ChildWerkloosheidswet by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWerkloosheidswet requireOne(ConnectionInterface $con = null) Return the first ChildWerkloosheidswet matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWerkloosheidswet requireOneById(int $id) Return the first ChildWerkloosheidswet filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWerkloosheidswet requireOneByPersoonId(int $persoon_id) Return the first ChildWerkloosheidswet filtered by the persoon_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWerkloosheidswet requireOneByBedrag(string $bedrag) Return the first ChildWerkloosheidswet filtered by the bedrag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWerkloosheidswet requireOneByStartDatum(string $start_datum) Return the first ChildWerkloosheidswet filtered by the start_datum column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWerkloosheidswet[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildWerkloosheidswet objects based on current ModelCriteria
 * @method     ChildWerkloosheidswet[]|ObjectCollection findById(int $id) Return ChildWerkloosheidswet objects filtered by the id column
 * @method     ChildWerkloosheidswet[]|ObjectCollection findByPersoonId(int $persoon_id) Return ChildWerkloosheidswet objects filtered by the persoon_id column
 * @method     ChildWerkloosheidswet[]|ObjectCollection findByBedrag(string $bedrag) Return ChildWerkloosheidswet objects filtered by the bedrag column
 * @method     ChildWerkloosheidswet[]|ObjectCollection findByStartDatum(string $start_datum) Return ChildWerkloosheidswet objects filtered by the start_datum column
 * @method     ChildWerkloosheidswet[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class WerkloosheidswetQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Custom\NovumUwv\Base\WerkloosheidswetQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Custom\\NovumUwv\\Werkloosheidswet', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildWerkloosheidswetQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildWerkloosheidswetQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildWerkloosheidswetQuery) {
            return $criteria;
        }
        $query = new ChildWerkloosheidswetQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildWerkloosheidswet|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(WerkloosheidswetTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = WerkloosheidswetTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWerkloosheidswet A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, persoon_id, bedrag, start_datum FROM werkloosheidswet WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildWerkloosheidswet $obj */
            $obj = new ChildWerkloosheidswet();
            $obj->hydrate($row);
            WerkloosheidswetTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildWerkloosheidswet|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildWerkloosheidswetQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(WerkloosheidswetTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildWerkloosheidswetQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(WerkloosheidswetTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWerkloosheidswetQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(WerkloosheidswetTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(WerkloosheidswetTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WerkloosheidswetTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the persoon_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPersoonId(1234); // WHERE persoon_id = 1234
     * $query->filterByPersoonId(array(12, 34)); // WHERE persoon_id IN (12, 34)
     * $query->filterByPersoonId(array('min' => 12)); // WHERE persoon_id > 12
     * </code>
     *
     * @see       filterByPersoon()
     *
     * @param     mixed $persoonId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWerkloosheidswetQuery The current query, for fluid interface
     */
    public function filterByPersoonId($persoonId = null, $comparison = null)
    {
        if (is_array($persoonId)) {
            $useMinMax = false;
            if (isset($persoonId['min'])) {
                $this->addUsingAlias(WerkloosheidswetTableMap::COL_PERSOON_ID, $persoonId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($persoonId['max'])) {
                $this->addUsingAlias(WerkloosheidswetTableMap::COL_PERSOON_ID, $persoonId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WerkloosheidswetTableMap::COL_PERSOON_ID, $persoonId, $comparison);
    }

    /**
     * Filter the query on the bedrag column
     *
     * Example usage:
     * <code>
     * $query->filterByBedrag('fooValue');   // WHERE bedrag = 'fooValue'
     * $query->filterByBedrag('%fooValue%', Criteria::LIKE); // WHERE bedrag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bedrag The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWerkloosheidswetQuery The current query, for fluid interface
     */
    public function filterByBedrag($bedrag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bedrag)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WerkloosheidswetTableMap::COL_BEDRAG, $bedrag, $comparison);
    }

    /**
     * Filter the query on the start_datum column
     *
     * Example usage:
     * <code>
     * $query->filterByStartDatum('fooValue');   // WHERE start_datum = 'fooValue'
     * $query->filterByStartDatum('%fooValue%', Criteria::LIKE); // WHERE start_datum LIKE '%fooValue%'
     * </code>
     *
     * @param     string $startDatum The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWerkloosheidswetQuery The current query, for fluid interface
     */
    public function filterByStartDatum($startDatum = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($startDatum)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WerkloosheidswetTableMap::COL_START_DATUM, $startDatum, $comparison);
    }

    /**
     * Filter the query by a related \Model\Custom\NovumUwv\Persoon object
     *
     * @param \Model\Custom\NovumUwv\Persoon|ObjectCollection $persoon The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWerkloosheidswetQuery The current query, for fluid interface
     */
    public function filterByPersoon($persoon, $comparison = null)
    {
        if ($persoon instanceof \Model\Custom\NovumUwv\Persoon) {
            return $this
                ->addUsingAlias(WerkloosheidswetTableMap::COL_PERSOON_ID, $persoon->getId(), $comparison);
        } elseif ($persoon instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WerkloosheidswetTableMap::COL_PERSOON_ID, $persoon->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPersoon() only accepts arguments of type \Model\Custom\NovumUwv\Persoon or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Persoon relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWerkloosheidswetQuery The current query, for fluid interface
     */
    public function joinPersoon($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Persoon');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Persoon');
        }

        return $this;
    }

    /**
     * Use the Persoon relation Persoon object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Custom\NovumUwv\PersoonQuery A secondary query class using the current class as primary query
     */
    public function usePersoonQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersoon($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Persoon', '\Model\Custom\NovumUwv\PersoonQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildWerkloosheidswet $werkloosheidswet Object to remove from the list of results
     *
     * @return $this|ChildWerkloosheidswetQuery The current query, for fluid interface
     */
    public function prune($werkloosheidswet = null)
    {
        if ($werkloosheidswet) {
            $this->addUsingAlias(WerkloosheidswetTableMap::COL_ID, $werkloosheidswet->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the werkloosheidswet table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WerkloosheidswetTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            WerkloosheidswetTableMap::clearInstancePool();
            WerkloosheidswetTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WerkloosheidswetTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(WerkloosheidswetTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            WerkloosheidswetTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            WerkloosheidswetTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // WerkloosheidswetQuery
