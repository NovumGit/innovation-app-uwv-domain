<?php

namespace Model\Custom\NovumUwv\Base;

use \Exception;
use \PDO;
use Model\Custom\NovumUwv\Persoon as ChildPersoon;
use Model\Custom\NovumUwv\PersoonQuery as ChildPersoonQuery;
use Model\Custom\NovumUwv\Map\PersoonTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'persoon' table.
 *
 * Alle bij het UWV bekendstaande personen
 *
 * @method     ChildPersoonQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPersoonQuery orderByBsn($order = Criteria::ASC) Order by the bsn column
 *
 * @method     ChildPersoonQuery groupById() Group by the id column
 * @method     ChildPersoonQuery groupByBsn() Group by the bsn column
 *
 * @method     ChildPersoonQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPersoonQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPersoonQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPersoonQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPersoonQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPersoonQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPersoonQuery leftJoinWerkloosheidswet($relationAlias = null) Adds a LEFT JOIN clause to the query using the Werkloosheidswet relation
 * @method     ChildPersoonQuery rightJoinWerkloosheidswet($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Werkloosheidswet relation
 * @method     ChildPersoonQuery innerJoinWerkloosheidswet($relationAlias = null) Adds a INNER JOIN clause to the query using the Werkloosheidswet relation
 *
 * @method     ChildPersoonQuery joinWithWerkloosheidswet($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Werkloosheidswet relation
 *
 * @method     ChildPersoonQuery leftJoinWithWerkloosheidswet() Adds a LEFT JOIN clause and with to the query using the Werkloosheidswet relation
 * @method     ChildPersoonQuery rightJoinWithWerkloosheidswet() Adds a RIGHT JOIN clause and with to the query using the Werkloosheidswet relation
 * @method     ChildPersoonQuery innerJoinWithWerkloosheidswet() Adds a INNER JOIN clause and with to the query using the Werkloosheidswet relation
 *
 * @method     ChildPersoonQuery leftJoinWao($relationAlias = null) Adds a LEFT JOIN clause to the query using the Wao relation
 * @method     ChildPersoonQuery rightJoinWao($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Wao relation
 * @method     ChildPersoonQuery innerJoinWao($relationAlias = null) Adds a INNER JOIN clause to the query using the Wao relation
 *
 * @method     ChildPersoonQuery joinWithWao($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Wao relation
 *
 * @method     ChildPersoonQuery leftJoinWithWao() Adds a LEFT JOIN clause and with to the query using the Wao relation
 * @method     ChildPersoonQuery rightJoinWithWao() Adds a RIGHT JOIN clause and with to the query using the Wao relation
 * @method     ChildPersoonQuery innerJoinWithWao() Adds a INNER JOIN clause and with to the query using the Wao relation
 *
 * @method     ChildPersoonQuery leftJoinWia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Wia relation
 * @method     ChildPersoonQuery rightJoinWia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Wia relation
 * @method     ChildPersoonQuery innerJoinWia($relationAlias = null) Adds a INNER JOIN clause to the query using the Wia relation
 *
 * @method     ChildPersoonQuery joinWithWia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Wia relation
 *
 * @method     ChildPersoonQuery leftJoinWithWia() Adds a LEFT JOIN clause and with to the query using the Wia relation
 * @method     ChildPersoonQuery rightJoinWithWia() Adds a RIGHT JOIN clause and with to the query using the Wia relation
 * @method     ChildPersoonQuery innerJoinWithWia() Adds a INNER JOIN clause and with to the query using the Wia relation
 *
 * @method     ChildPersoonQuery leftJoinIow($relationAlias = null) Adds a LEFT JOIN clause to the query using the Iow relation
 * @method     ChildPersoonQuery rightJoinIow($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Iow relation
 * @method     ChildPersoonQuery innerJoinIow($relationAlias = null) Adds a INNER JOIN clause to the query using the Iow relation
 *
 * @method     ChildPersoonQuery joinWithIow($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Iow relation
 *
 * @method     ChildPersoonQuery leftJoinWithIow() Adds a LEFT JOIN clause and with to the query using the Iow relation
 * @method     ChildPersoonQuery rightJoinWithIow() Adds a RIGHT JOIN clause and with to the query using the Iow relation
 * @method     ChildPersoonQuery innerJoinWithIow() Adds a INNER JOIN clause and with to the query using the Iow relation
 *
 * @method     ChildPersoonQuery leftJoinBijstand($relationAlias = null) Adds a LEFT JOIN clause to the query using the Bijstand relation
 * @method     ChildPersoonQuery rightJoinBijstand($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Bijstand relation
 * @method     ChildPersoonQuery innerJoinBijstand($relationAlias = null) Adds a INNER JOIN clause to the query using the Bijstand relation
 *
 * @method     ChildPersoonQuery joinWithBijstand($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Bijstand relation
 *
 * @method     ChildPersoonQuery leftJoinWithBijstand() Adds a LEFT JOIN clause and with to the query using the Bijstand relation
 * @method     ChildPersoonQuery rightJoinWithBijstand() Adds a RIGHT JOIN clause and with to the query using the Bijstand relation
 * @method     ChildPersoonQuery innerJoinWithBijstand() Adds a INNER JOIN clause and with to the query using the Bijstand relation
 *
 * @method     ChildPersoonQuery leftJoinwamil($relationAlias = null) Adds a LEFT JOIN clause to the query using the wamil relation
 * @method     ChildPersoonQuery rightJoinwamil($relationAlias = null) Adds a RIGHT JOIN clause to the query using the wamil relation
 * @method     ChildPersoonQuery innerJoinwamil($relationAlias = null) Adds a INNER JOIN clause to the query using the wamil relation
 *
 * @method     ChildPersoonQuery joinWithwamil($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the wamil relation
 *
 * @method     ChildPersoonQuery leftJoinWithwamil() Adds a LEFT JOIN clause and with to the query using the wamil relation
 * @method     ChildPersoonQuery rightJoinWithwamil() Adds a RIGHT JOIN clause and with to the query using the wamil relation
 * @method     ChildPersoonQuery innerJoinWithwamil() Adds a INNER JOIN clause and with to the query using the wamil relation
 *
 * @method     \Model\Custom\NovumUwv\WerkloosheidswetQuery|\Model\Custom\NovumUwv\WaoQuery|\Model\Custom\NovumUwv\WiaQuery|\Model\Custom\NovumUwv\IowQuery|\Model\Custom\NovumUwv\BijstandQuery|\Model\Custom\NovumUwv\wamilQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPersoon findOne(ConnectionInterface $con = null) Return the first ChildPersoon matching the query
 * @method     ChildPersoon findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPersoon matching the query, or a new ChildPersoon object populated from the query conditions when no match is found
 *
 * @method     ChildPersoon findOneById(int $id) Return the first ChildPersoon filtered by the id column
 * @method     ChildPersoon findOneByBsn(string $bsn) Return the first ChildPersoon filtered by the bsn column *

 * @method     ChildPersoon requirePk($key, ConnectionInterface $con = null) Return the ChildPersoon by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersoon requireOne(ConnectionInterface $con = null) Return the first ChildPersoon matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPersoon requireOneById(int $id) Return the first ChildPersoon filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersoon requireOneByBsn(string $bsn) Return the first ChildPersoon filtered by the bsn column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPersoon[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPersoon objects based on current ModelCriteria
 * @method     ChildPersoon[]|ObjectCollection findById(int $id) Return ChildPersoon objects filtered by the id column
 * @method     ChildPersoon[]|ObjectCollection findByBsn(string $bsn) Return ChildPersoon objects filtered by the bsn column
 * @method     ChildPersoon[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PersoonQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Custom\NovumUwv\Base\PersoonQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Custom\\NovumUwv\\Persoon', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPersoonQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPersoonQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPersoonQuery) {
            return $criteria;
        }
        $query = new ChildPersoonQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPersoon|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PersoonTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PersoonTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPersoon A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, bsn FROM persoon WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPersoon $obj */
            $obj = new ChildPersoon();
            $obj->hydrate($row);
            PersoonTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPersoon|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPersoonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PersoonTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPersoonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PersoonTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersoonQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PersoonTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PersoonTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersoonTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the bsn column
     *
     * Example usage:
     * <code>
     * $query->filterByBsn('fooValue');   // WHERE bsn = 'fooValue'
     * $query->filterByBsn('%fooValue%', Criteria::LIKE); // WHERE bsn LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bsn The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersoonQuery The current query, for fluid interface
     */
    public function filterByBsn($bsn = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bsn)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersoonTableMap::COL_BSN, $bsn, $comparison);
    }

    /**
     * Filter the query by a related \Model\Custom\NovumUwv\Werkloosheidswet object
     *
     * @param \Model\Custom\NovumUwv\Werkloosheidswet|ObjectCollection $werkloosheidswet the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersoonQuery The current query, for fluid interface
     */
    public function filterByWerkloosheidswet($werkloosheidswet, $comparison = null)
    {
        if ($werkloosheidswet instanceof \Model\Custom\NovumUwv\Werkloosheidswet) {
            return $this
                ->addUsingAlias(PersoonTableMap::COL_ID, $werkloosheidswet->getPersoonId(), $comparison);
        } elseif ($werkloosheidswet instanceof ObjectCollection) {
            return $this
                ->useWerkloosheidswetQuery()
                ->filterByPrimaryKeys($werkloosheidswet->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWerkloosheidswet() only accepts arguments of type \Model\Custom\NovumUwv\Werkloosheidswet or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Werkloosheidswet relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersoonQuery The current query, for fluid interface
     */
    public function joinWerkloosheidswet($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Werkloosheidswet');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Werkloosheidswet');
        }

        return $this;
    }

    /**
     * Use the Werkloosheidswet relation Werkloosheidswet object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Custom\NovumUwv\WerkloosheidswetQuery A secondary query class using the current class as primary query
     */
    public function useWerkloosheidswetQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWerkloosheidswet($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Werkloosheidswet', '\Model\Custom\NovumUwv\WerkloosheidswetQuery');
    }

    /**
     * Filter the query by a related \Model\Custom\NovumUwv\Wao object
     *
     * @param \Model\Custom\NovumUwv\Wao|ObjectCollection $wao the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersoonQuery The current query, for fluid interface
     */
    public function filterByWao($wao, $comparison = null)
    {
        if ($wao instanceof \Model\Custom\NovumUwv\Wao) {
            return $this
                ->addUsingAlias(PersoonTableMap::COL_ID, $wao->getPersoonId(), $comparison);
        } elseif ($wao instanceof ObjectCollection) {
            return $this
                ->useWaoQuery()
                ->filterByPrimaryKeys($wao->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWao() only accepts arguments of type \Model\Custom\NovumUwv\Wao or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Wao relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersoonQuery The current query, for fluid interface
     */
    public function joinWao($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Wao');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Wao');
        }

        return $this;
    }

    /**
     * Use the Wao relation Wao object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Custom\NovumUwv\WaoQuery A secondary query class using the current class as primary query
     */
    public function useWaoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWao($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Wao', '\Model\Custom\NovumUwv\WaoQuery');
    }

    /**
     * Filter the query by a related \Model\Custom\NovumUwv\Wia object
     *
     * @param \Model\Custom\NovumUwv\Wia|ObjectCollection $wia the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersoonQuery The current query, for fluid interface
     */
    public function filterByWia($wia, $comparison = null)
    {
        if ($wia instanceof \Model\Custom\NovumUwv\Wia) {
            return $this
                ->addUsingAlias(PersoonTableMap::COL_ID, $wia->getPersoonId(), $comparison);
        } elseif ($wia instanceof ObjectCollection) {
            return $this
                ->useWiaQuery()
                ->filterByPrimaryKeys($wia->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWia() only accepts arguments of type \Model\Custom\NovumUwv\Wia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Wia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersoonQuery The current query, for fluid interface
     */
    public function joinWia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Wia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Wia');
        }

        return $this;
    }

    /**
     * Use the Wia relation Wia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Custom\NovumUwv\WiaQuery A secondary query class using the current class as primary query
     */
    public function useWiaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Wia', '\Model\Custom\NovumUwv\WiaQuery');
    }

    /**
     * Filter the query by a related \Model\Custom\NovumUwv\Iow object
     *
     * @param \Model\Custom\NovumUwv\Iow|ObjectCollection $iow the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersoonQuery The current query, for fluid interface
     */
    public function filterByIow($iow, $comparison = null)
    {
        if ($iow instanceof \Model\Custom\NovumUwv\Iow) {
            return $this
                ->addUsingAlias(PersoonTableMap::COL_ID, $iow->getPersoonId(), $comparison);
        } elseif ($iow instanceof ObjectCollection) {
            return $this
                ->useIowQuery()
                ->filterByPrimaryKeys($iow->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByIow() only accepts arguments of type \Model\Custom\NovumUwv\Iow or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Iow relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersoonQuery The current query, for fluid interface
     */
    public function joinIow($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Iow');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Iow');
        }

        return $this;
    }

    /**
     * Use the Iow relation Iow object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Custom\NovumUwv\IowQuery A secondary query class using the current class as primary query
     */
    public function useIowQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinIow($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Iow', '\Model\Custom\NovumUwv\IowQuery');
    }

    /**
     * Filter the query by a related \Model\Custom\NovumUwv\Bijstand object
     *
     * @param \Model\Custom\NovumUwv\Bijstand|ObjectCollection $bijstand the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersoonQuery The current query, for fluid interface
     */
    public function filterByBijstand($bijstand, $comparison = null)
    {
        if ($bijstand instanceof \Model\Custom\NovumUwv\Bijstand) {
            return $this
                ->addUsingAlias(PersoonTableMap::COL_ID, $bijstand->getPersoonId(), $comparison);
        } elseif ($bijstand instanceof ObjectCollection) {
            return $this
                ->useBijstandQuery()
                ->filterByPrimaryKeys($bijstand->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBijstand() only accepts arguments of type \Model\Custom\NovumUwv\Bijstand or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Bijstand relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersoonQuery The current query, for fluid interface
     */
    public function joinBijstand($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Bijstand');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Bijstand');
        }

        return $this;
    }

    /**
     * Use the Bijstand relation Bijstand object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Custom\NovumUwv\BijstandQuery A secondary query class using the current class as primary query
     */
    public function useBijstandQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBijstand($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Bijstand', '\Model\Custom\NovumUwv\BijstandQuery');
    }

    /**
     * Filter the query by a related \Model\Custom\NovumUwv\wamil object
     *
     * @param \Model\Custom\NovumUwv\wamil|ObjectCollection $wamil the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersoonQuery The current query, for fluid interface
     */
    public function filterBywamil($wamil, $comparison = null)
    {
        if ($wamil instanceof \Model\Custom\NovumUwv\wamil) {
            return $this
                ->addUsingAlias(PersoonTableMap::COL_ID, $wamil->getPersoonId(), $comparison);
        } elseif ($wamil instanceof ObjectCollection) {
            return $this
                ->usewamilQuery()
                ->filterByPrimaryKeys($wamil->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBywamil() only accepts arguments of type \Model\Custom\NovumUwv\wamil or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the wamil relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersoonQuery The current query, for fluid interface
     */
    public function joinwamil($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('wamil');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'wamil');
        }

        return $this;
    }

    /**
     * Use the wamil relation wamil object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Custom\NovumUwv\wamilQuery A secondary query class using the current class as primary query
     */
    public function usewamilQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinwamil($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'wamil', '\Model\Custom\NovumUwv\wamilQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPersoon $persoon Object to remove from the list of results
     *
     * @return $this|ChildPersoonQuery The current query, for fluid interface
     */
    public function prune($persoon = null)
    {
        if ($persoon) {
            $this->addUsingAlias(PersoonTableMap::COL_ID, $persoon->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the persoon table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PersoonTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PersoonTableMap::clearInstancePool();
            PersoonTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PersoonTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PersoonTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PersoonTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PersoonTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PersoonQuery
