<?php

namespace Model\Custom\NovumUwv;

use Model\Custom\NovumUwv\Base\Werkloosheidswet as BaseWerkloosheidswet;

/**
 * Skeleton subclass for representing a row from the 'werkloosheidswet' table.
 *
 * Bevat alle personen die gebruik maken van de werkloosheidswet
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Werkloosheidswet extends BaseWerkloosheidswet
{

}
