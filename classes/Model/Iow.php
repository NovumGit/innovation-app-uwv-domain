<?php

namespace Model\Custom\NovumUwv;

use Model\Custom\NovumUwv\Base\Iow as BaseIow;

/**
 * Skeleton subclass for representing a row from the 'iow' table.
 *
 * Bevat alle personen die gebruik maken van de wet inkomensvoorziening oudere werklozen
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Iow extends BaseIow
{

}
