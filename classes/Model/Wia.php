<?php

namespace Model\Custom\NovumUwv;

use Model\Custom\NovumUwv\Base\Wia as BaseWia;

/**
 * Skeleton subclass for representing a row from the 'wia' table.
 *
 * Bevat alle personen die gebruik maken van de WIA regeling
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Wia extends BaseWia
{

}
