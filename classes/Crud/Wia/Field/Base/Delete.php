<?php 
namespace Crud\Custom\NovumUwv\Wia\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumUwv\Wia;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Wia)
		{
		     return "/custom/novumuwv/arbeidsongeschiktheid/wia/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Wia)
		{
		     return "/custom/novumuwv/wia?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
