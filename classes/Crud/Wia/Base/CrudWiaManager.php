<?php
namespace Crud\Custom\NovumUwv\Wia\Base;

use Crud\Custom\NovumUwv;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumUwv\Map\WiaTableMap;
use Model\Custom\NovumUwv\Wia;
use Model\Custom\NovumUwv\WiaQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Wia instead if you need to override or add functionality.
 */
abstract class CrudWiaManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumUwv\CrudTrait;
	use NovumUwv\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return WiaQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumUwv\Map\WiaTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat alle personen die gebruik maken van de WIA regeling";
	}


	public function getEntityTitle(): string
	{
		return "Wia";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumuwv/arbeidsongeschiktheid/wia/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumuwv/arbeidsongeschiktheid/wia/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Wia toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Wia aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Wia
	 */
	public function getModel(array $aData = null): Wia
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oWiaQuery = WiaQuery::create();
		     $oWia = $oWiaQuery->findOneById($aData['id']);
		     if (!$oWia instanceof Wia) {
		         throw new LogicException("Wia should be an instance of Wia but got something else." . __METHOD__);
		     }
		     $oWia = $this->fillVo($aData, $oWia);
		} else {
		     $oWia = new Wia();
		     if (!empty($aData)) {
		         $oWia = $this->fillVo($aData, $oWia);
		     }
		}
		return $oWia;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Wia
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Wia
	{
		$oWia = $this->getModel($aData);


		 if(!empty($oWia))
		 {
		     $oWia = $this->fillVo($aData, $oWia);
		     $oWia->save();
		 }
		return $oWia;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Wia $oModel
	 * @return Wia
	 */
	protected function fillVo(array $aData, Wia $oModel): Wia
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['bedrag']) ? $oModel->setBedrag($aData['bedrag']) : null;
		isset($aData['start_datum']) ? $oModel->setStartDatum($aData['start_datum']) : null;
		return $oModel;
	}
}
