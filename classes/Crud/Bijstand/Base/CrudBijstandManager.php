<?php
namespace Crud\Custom\NovumUwv\Bijstand\Base;

use Crud\Custom\NovumUwv;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumUwv\Bijstand;
use Model\Custom\NovumUwv\BijstandQuery;
use Model\Custom\NovumUwv\Map\BijstandTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Bijstand instead if you need to override or add functionality.
 */
abstract class CrudBijstandManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumUwv\CrudTrait;
	use NovumUwv\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return BijstandQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumUwv\Map\BijstandTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat alle personen die gebruik maken van de bijstandwet";
	}


	public function getEntityTitle(): string
	{
		return "Bijstand";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumuwv/werkloosheid/bijstand/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumuwv/werkloosheid/bijstand/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Bijstandswet toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Bijstandswet aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Bijstand
	 */
	public function getModel(array $aData = null): Bijstand
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBijstandQuery = BijstandQuery::create();
		     $oBijstand = $oBijstandQuery->findOneById($aData['id']);
		     if (!$oBijstand instanceof Bijstand) {
		         throw new LogicException("Bijstand should be an instance of Bijstand but got something else." . __METHOD__);
		     }
		     $oBijstand = $this->fillVo($aData, $oBijstand);
		} else {
		     $oBijstand = new Bijstand();
		     if (!empty($aData)) {
		         $oBijstand = $this->fillVo($aData, $oBijstand);
		     }
		}
		return $oBijstand;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Bijstand
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Bijstand
	{
		$oBijstand = $this->getModel($aData);


		 if(!empty($oBijstand))
		 {
		     $oBijstand = $this->fillVo($aData, $oBijstand);
		     $oBijstand->save();
		 }
		return $oBijstand;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Bijstand $oModel
	 * @return Bijstand
	 */
	protected function fillVo(array $aData, Bijstand $oModel): Bijstand
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['bedrag']) ? $oModel->setBedrag($aData['bedrag']) : null;
		isset($aData['start_datum']) ? $oModel->setStartDatum($aData['start_datum']) : null;
		return $oModel;
	}
}
