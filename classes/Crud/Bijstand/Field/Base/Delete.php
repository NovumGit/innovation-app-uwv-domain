<?php 
namespace Crud\Custom\NovumUwv\Bijstand\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumUwv\Bijstand;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Bijstand)
		{
		     return "/custom/novumuwv/werkloosheid/bijstand/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Bijstand)
		{
		     return "/custom/novumuwv/bijstand?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
