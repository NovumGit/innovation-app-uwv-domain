<?php
namespace Crud\Custom\NovumUwv\Persoon\Base;

use Crud\Custom\NovumUwv;
use Crud\FormManager;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumUwv\Map\PersoonTableMap;
use Model\Custom\NovumUwv\Persoon;
use Model\Custom\NovumUwv\PersoonQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Persoon instead if you need to override or add functionality.
 */
abstract class CrudPersoonManager extends FormManager implements IConfigurableCrud
{
	use NovumUwv\CrudTrait;
	use NovumUwv\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return PersoonQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumUwv\Map\PersoonTableMap();
	}


	public function getShortDescription(): string
	{
		return "Alle bij het UWV bekendstaande personen";
	}


	public function getEntityTitle(): string
	{
		return "Persoon";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumuwv/persoonsgegevens/persoon/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumuwv/persoonsgegevens/persoon/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Persoon toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Persoon aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['Bsn', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['Bsn'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Persoon
	 */
	public function getModel(array $aData = null): Persoon
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oPersoonQuery = PersoonQuery::create();
		     $oPersoon = $oPersoonQuery->findOneById($aData['id']);
		     if (!$oPersoon instanceof Persoon) {
		         throw new LogicException("Persoon should be an instance of Persoon but got something else." . __METHOD__);
		     }
		     $oPersoon = $this->fillVo($aData, $oPersoon);
		} else {
		     $oPersoon = new Persoon();
		     if (!empty($aData)) {
		         $oPersoon = $this->fillVo($aData, $oPersoon);
		     }
		}
		return $oPersoon;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Persoon
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Persoon
	{
		$oPersoon = $this->getModel($aData);


		 if(!empty($oPersoon))
		 {
		     $oPersoon = $this->fillVo($aData, $oPersoon);
		     $oPersoon->save();
		 }
		return $oPersoon;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Persoon $oModel
	 * @return Persoon
	 */
	protected function fillVo(array $aData, Persoon $oModel): Persoon
	{
		isset($aData['bsn']) ? $oModel->setBsn($aData['bsn']) : null;
		return $oModel;
	}
}
