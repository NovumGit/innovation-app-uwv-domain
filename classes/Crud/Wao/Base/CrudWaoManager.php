<?php
namespace Crud\Custom\NovumUwv\Wao\Base;

use Crud\Custom\NovumUwv;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumUwv\Map\WaoTableMap;
use Model\Custom\NovumUwv\Wao;
use Model\Custom\NovumUwv\WaoQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Wao instead if you need to override or add functionality.
 */
abstract class CrudWaoManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumUwv\CrudTrait;
	use NovumUwv\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return WaoQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumUwv\Map\WaoTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat alle personen die gebruik maken van de wet arbeidsongeschiktheid";
	}


	public function getEntityTitle(): string
	{
		return "Wao";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumuwv/arbeidsongeschiktheid/wao/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumuwv/arbeidsongeschiktheid/wao/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Wet arbeidsongeschiktheid toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Wet arbeidsongeschiktheid aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Wao
	 */
	public function getModel(array $aData = null): Wao
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oWaoQuery = WaoQuery::create();
		     $oWao = $oWaoQuery->findOneById($aData['id']);
		     if (!$oWao instanceof Wao) {
		         throw new LogicException("Wao should be an instance of Wao but got something else." . __METHOD__);
		     }
		     $oWao = $this->fillVo($aData, $oWao);
		} else {
		     $oWao = new Wao();
		     if (!empty($aData)) {
		         $oWao = $this->fillVo($aData, $oWao);
		     }
		}
		return $oWao;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Wao
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Wao
	{
		$oWao = $this->getModel($aData);


		 if(!empty($oWao))
		 {
		     $oWao = $this->fillVo($aData, $oWao);
		     $oWao->save();
		 }
		return $oWao;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Wao $oModel
	 * @return Wao
	 */
	protected function fillVo(array $aData, Wao $oModel): Wao
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['bedrag']) ? $oModel->setBedrag($aData['bedrag']) : null;
		isset($aData['start_datum']) ? $oModel->setStartDatum($aData['start_datum']) : null;
		return $oModel;
	}
}
