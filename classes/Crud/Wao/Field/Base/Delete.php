<?php 
namespace Crud\Custom\NovumUwv\Wao\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumUwv\Wao;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Wao)
		{
		     return "/custom/novumuwv/arbeidsongeschiktheid/wao/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Wao)
		{
		     return "/custom/novumuwv/wao?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
