<?php 
namespace Crud\Custom\NovumUwv\Iow\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumUwv\Iow;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Iow)
		{
		     return "/custom/novumuwv/werkloosheid/iow/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Iow)
		{
		     return "/custom/novumuwv/iow?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
