<?php
namespace Crud\Custom\NovumUwv\Iow\Base;

use Crud\Custom\NovumUwv;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumUwv\Iow;
use Model\Custom\NovumUwv\IowQuery;
use Model\Custom\NovumUwv\Map\IowTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Iow instead if you need to override or add functionality.
 */
abstract class CrudIowManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumUwv\CrudTrait;
	use NovumUwv\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return IowQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumUwv\Map\IowTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat alle personen die gebruik maken van de wet inkomensvoorziening oudere werklozen";
	}


	public function getEntityTitle(): string
	{
		return "Iow";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumuwv/werkloosheid/iow/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumuwv/werkloosheid/iow/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Wet inkomensvoorziening oudere werklozen toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Wet inkomensvoorziening oudere werklozen aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Iow
	 */
	public function getModel(array $aData = null): Iow
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oIowQuery = IowQuery::create();
		     $oIow = $oIowQuery->findOneById($aData['id']);
		     if (!$oIow instanceof Iow) {
		         throw new LogicException("Iow should be an instance of Iow but got something else." . __METHOD__);
		     }
		     $oIow = $this->fillVo($aData, $oIow);
		} else {
		     $oIow = new Iow();
		     if (!empty($aData)) {
		         $oIow = $this->fillVo($aData, $oIow);
		     }
		}
		return $oIow;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Iow
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Iow
	{
		$oIow = $this->getModel($aData);


		 if(!empty($oIow))
		 {
		     $oIow = $this->fillVo($aData, $oIow);
		     $oIow->save();
		 }
		return $oIow;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Iow $oModel
	 * @return Iow
	 */
	protected function fillVo(array $aData, Iow $oModel): Iow
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['bedrag']) ? $oModel->setBedrag($aData['bedrag']) : null;
		isset($aData['start_datum']) ? $oModel->setStartDatum($aData['start_datum']) : null;
		return $oModel;
	}
}
