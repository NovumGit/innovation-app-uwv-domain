<?php 
namespace Crud\Custom\NovumUwv\wamil\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumUwv\wamil;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof wamil)
		{
		     return "/custom/novumuwv/arbeidsongeschiktheid/wamil/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof wamil)
		{
		     return "/custom/novumuwv/wamil?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
