<?php
namespace Crud\Custom\NovumUwv\wamil\Field;

use Crud\Custom\NovumUwv\wamil\Field\Base\Bedrag as BaseBedrag;

/**
 * Skeleton subclass for representing bedrag field from the wamil table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Bedrag extends BaseBedrag
{
}
