<?php
namespace Crud\Custom\NovumUwv\wamil\Base;

use Crud\Custom\NovumUwv;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumUwv\Map\wamilTableMap;
use Model\Custom\NovumUwv\wamil;
use Model\Custom\NovumUwv\wamilQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify wamil instead if you need to override or add functionality.
 */
abstract class CrudwamilManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumUwv\CrudTrait;
	use NovumUwv\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return wamilQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumUwv\Map\wamilTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat alle personen die gebruik maken van de wet arbeidsongeschiktheidsvoorziening militairen";
	}


	public function getEntityTitle(): string
	{
		return "wamil";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumuwv/arbeidsongeschiktheid/wamil/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumuwv/arbeidsongeschiktheid/wamil/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Wamil toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Wamil aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return wamil
	 */
	public function getModel(array $aData = null): wamil
	{
		if (isset($aData['id']) && $aData['id']) {
		     $owamilQuery = wamilQuery::create();
		     $owamil = $owamilQuery->findOneById($aData['id']);
		     if (!$owamil instanceof wamil) {
		         throw new LogicException("wamil should be an instance of wamil but got something else." . __METHOD__);
		     }
		     $owamil = $this->fillVo($aData, $owamil);
		} else {
		     $owamil = new wamil();
		     if (!empty($aData)) {
		         $owamil = $this->fillVo($aData, $owamil);
		     }
		}
		return $owamil;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return wamil
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): wamil
	{
		$owamil = $this->getModel($aData);


		 if(!empty($owamil))
		 {
		     $owamil = $this->fillVo($aData, $owamil);
		     $owamil->save();
		 }
		return $owamil;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param wamil $oModel
	 * @return wamil
	 */
	protected function fillVo(array $aData, wamil $oModel): wamil
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['bedrag']) ? $oModel->setBedrag($aData['bedrag']) : null;
		isset($aData['start_datum']) ? $oModel->setStartDatum($aData['start_datum']) : null;
		return $oModel;
	}
}
