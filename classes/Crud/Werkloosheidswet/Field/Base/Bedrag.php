<?php
namespace Crud\Custom\NovumUwv\Werkloosheidswet\Field\Base;

use Crud\Generic\Field\GenericMoney;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'bedrag' crud field from the 'werkloosheidswet' table.
 * This class is auto generated and should not be modified.
 */
abstract class Bedrag extends GenericMoney implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'bedrag';

	protected $sFieldLabel = 'Bedrag';

	protected $sIcon = 'money';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getBedrag';

	protected $sFqModelClassname = '\Model\Custom\NovumUwv\Werkloosheidswet';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['bedrag']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Bedrag" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
