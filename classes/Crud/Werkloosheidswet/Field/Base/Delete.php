<?php 
namespace Crud\Custom\NovumUwv\Werkloosheidswet\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumUwv\Werkloosheidswet;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Werkloosheidswet)
		{
		     return "/custom/novumuwv/werkloosheid/werkloosheidswet/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Werkloosheidswet)
		{
		     return "/custom/novumuwv/werkloosheidswet?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
