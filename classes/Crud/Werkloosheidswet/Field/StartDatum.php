<?php
namespace Crud\Custom\NovumUwv\Werkloosheidswet\Field;

use Crud\Custom\NovumUwv\Werkloosheidswet\Field\Base\StartDatum as BaseStartDatum;

/**
 * Skeleton subclass for representing start_datum field from the werkloosheidswet table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class StartDatum extends BaseStartDatum
{
}
