<?php
namespace Crud\Custom\NovumUwv\Werkloosheidswet\Base;

use Crud\Custom\NovumUwv;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumUwv\Map\WerkloosheidswetTableMap;
use Model\Custom\NovumUwv\Werkloosheidswet;
use Model\Custom\NovumUwv\WerkloosheidswetQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Werkloosheidswet instead if you need to override or add functionality.
 */
abstract class CrudWerkloosheidswetManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumUwv\CrudTrait;
	use NovumUwv\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return WerkloosheidswetQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumUwv\Map\WerkloosheidswetTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat alle personen die gebruik maken van de werkloosheidswet";
	}


	public function getEntityTitle(): string
	{
		return "Werkloosheidswet";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumuwv/werkloosheid/werkloosheidswet/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumuwv/werkloosheid/werkloosheidswet/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Werkloosheidswet toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Werkloosheidswet aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['PersoonId', 'Bedrag', 'StartDatum'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Werkloosheidswet
	 */
	public function getModel(array $aData = null): Werkloosheidswet
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oWerkloosheidswetQuery = WerkloosheidswetQuery::create();
		     $oWerkloosheidswet = $oWerkloosheidswetQuery->findOneById($aData['id']);
		     if (!$oWerkloosheidswet instanceof Werkloosheidswet) {
		         throw new LogicException("Werkloosheidswet should be an instance of Werkloosheidswet but got something else." . __METHOD__);
		     }
		     $oWerkloosheidswet = $this->fillVo($aData, $oWerkloosheidswet);
		} else {
		     $oWerkloosheidswet = new Werkloosheidswet();
		     if (!empty($aData)) {
		         $oWerkloosheidswet = $this->fillVo($aData, $oWerkloosheidswet);
		     }
		}
		return $oWerkloosheidswet;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Werkloosheidswet
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Werkloosheidswet
	{
		$oWerkloosheidswet = $this->getModel($aData);


		 if(!empty($oWerkloosheidswet))
		 {
		     $oWerkloosheidswet = $this->fillVo($aData, $oWerkloosheidswet);
		     $oWerkloosheidswet->save();
		 }
		return $oWerkloosheidswet;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Werkloosheidswet $oModel
	 * @return Werkloosheidswet
	 */
	protected function fillVo(array $aData, Werkloosheidswet $oModel): Werkloosheidswet
	{
		isset($aData['persoon_id']) ? $oModel->setPersoonId($aData['persoon_id']) : null;
		isset($aData['bedrag']) ? $oModel->setBedrag($aData['bedrag']) : null;
		isset($aData['start_datum']) ? $oModel->setStartDatum($aData['start_datum']) : null;
		return $oModel;
	}
}
